<img src="https://i.imgur.com/vonwIAk.jpg">

# About this repo
<h4>In this repo, there are all the English strings of the site and Bot of [Bottino](https://t.me/BottinoBot) and [ChannelBot](https://t.me/ChannelBot), as well as the update pages of both them.</h4>


## Index
- <h4>[Bots strings](https://gitlab.com/alsoGAMER/bottino-strings-en/-/tree/master/strings)</h4>
- <h4>[Website strings](https://gitlab.com/alsoGAMER/bottino-strings-en/-/tree/master/website)</h4>
- <h4>[Updates pages](https://gitlab.com/alsoGAMER/bottino-strings-en/-/tree/master/updates)</h4>

## Credits
- 🇺🇸 Translator: @alsoGAMER
- 🚀 Developer: @bottinotg

## Notes
- This repo will remain online and public for historical reference. No commits will be done from now on.<br /><br />R.I.P. Bottino 2018-2020 ❤️